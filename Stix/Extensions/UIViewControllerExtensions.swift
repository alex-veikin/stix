//
//  UIViewControllerExtensions.swift
//

import UIKit

// Hide keyboard when tap outside keyboard
//
// Add "self.dismissKey()" in "viewDidLoad()" method
extension UIViewController {
    func dismissKey() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer( target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

// Add/remove scroll for scroll-view when show/hide keyboard
//
// Add "self.addKeyboardObservers()" in "viewDidLoad()" method
extension UIViewController {
    func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let view = self.view as! UIScrollView
        let keyboardFrame = keyboardValue.cgRectValue
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            view.contentSize = CGSize(width: view.bounds.size.width, height: view.bounds.size.height)
        } else {
            view.contentSize = CGSize(width: view.bounds.size.width, height: view.bounds.size.height + keyboardFrame.size.height)
        }
        
        view.scrollIndicatorInsets.bottom = keyboardFrame.size.height
    }
}
