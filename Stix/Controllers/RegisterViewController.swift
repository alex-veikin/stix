import UIKit
import Firebase

class RegisterViewController: UIViewController {
    
    var ref: DatabaseReference!
    
    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ref = Database.database().reference()
        
        warningLabel.alpha = 0
        
        self.addKeyboardObservers()
        self.dismissKey()
    }
    
    @IBAction func registerPressed(_ sender: UIButton) {
        self.registerButton.isEnabled = false
        guard let email = emailTF.text, let password = passwordTF.text, !email.isEmpty, !password.isEmpty else {
            if emailTF.text!.isEmpty  {
                showWarningLabel(withText: "The email field must not be empty")
            } else if passwordTF.text!.isEmpty  {
                showWarningLabel(withText: "The password field must not be empty")
            }
            self.registerButton.isEnabled = true
            return
        }
        
        Auth.auth().createUser(withEmail: email, password: password) {
            [weak self] auth, error in
            guard error == nil, auth?.user != nil else {
                self?.showWarningLabel(withText: "Registration error: \(error!.localizedDescription)")
                self?.registerButton.isEnabled = true
                return
            }
            
            self?.ref.child("users/\((auth?.user.uid)!)/email").setValue(auth?.user.email)
            
            self?.performSegue(withIdentifier: "loginPageSegue", sender: nil)
        }
    }
    
    func showWarningLabel(withText text: String) {
        warningLabel.text = text
        
        UIView.animate(withDuration: 3, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 1, options: .curveEaseInOut, animations: { [weak self] in self?.warningLabel.alpha = 1 }, completion: { [weak self] complete in self?.warningLabel.alpha = 0 })
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
