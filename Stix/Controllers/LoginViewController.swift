import UIKit
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        warningLabel.alpha = 0
        
        self.checkAuth()
        self.addKeyboardObservers()
        self.dismissKey()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.checkAuth()
        
        emailTF.text = ""
        passwordTF.text = ""
    }

    @IBAction func loginPressed(_ sender: UIButton) {
        guard let email = emailTF.text, let password = passwordTF.text, !email.isEmpty, !password.isEmpty else {
            if emailTF.text!.isEmpty  {
                showWarningLabel(withText: "The email field must not be empty")
            } else if passwordTF.text!.isEmpty  {
                showWarningLabel(withText: "The password field must not be empty")
            }
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) {
            [weak self] user, error in
            guard error == nil, user != nil else {
                self?.showWarningLabel(withText: "Authentication error: \(error!.localizedDescription)")
                return
            }
            
            self?.performSegue(withIdentifier: "foldersSegue", sender: nil)
        }
    }
    
    func checkAuth() {
        Auth.auth().addStateDidChangeListener({ [weak self] auth, user in
            if user != nil {
                self?.performSegue(withIdentifier: "foldersSegue", sender: nil)
            }
        })
    }
    
    func showWarningLabel(withText text: String) {
        warningLabel.text = text
        
        UIView.animate(withDuration: 3, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 1, options: .curveEaseInOut, animations: { [weak self] in self?.warningLabel.alpha = 1 }, completion: { [weak self] complete in self?.warningLabel.alpha = 0 })
    }
}

