import UIKit
import Firebase

class FoldersViewController: UIViewController {
    
    var user: User!
    var ref: DatabaseReference!
    var folders = [Folder]()
    
    @IBOutlet weak var foldersTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let currentUser = Auth.auth().currentUser else { return }
        self.user = User(user: currentUser)
        self.ref = Database.database().reference(withPath: "users/\(user.uid)/folders")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.ref.observe(.value, with: { [weak self] snapshot in
            var folders = [Folder]()
            for item in snapshot.children {
                let folder = Folder(snapshot: item as! DataSnapshot)
                folders.append(folder as Folder)
            }
            self?.folders = folders
            self?.foldersTableView.reloadData()
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.ref.removeAllObservers()
    }
    
    @IBAction func signOutPressed(_ sender: UIBarButtonItem) {
        do {
            try Auth.auth().signOut()
        } catch {
            print(error.localizedDescription)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addPressed(_ sender: UIBarButtonItem) {
        let ac = UIAlertController(title: "Add folder", message: "Add new folder", preferredStyle: .alert)
        ac.addTextField(configurationHandler: nil)
        
        let saveButton = UIAlertAction(title: "Save", style: .default) { [weak self] _ in
            guard let textField = ac.textFields?.first, !textField.text!.isEmpty else { return }
            let folder = Folder(title: textField.text!)
            let folderRef = self?.ref.childByAutoId()
            folderRef?.setValue(["title": folder.title])
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        ac.addAction(saveButton)
        ac.addAction(cancelButton)
        
        present(ac, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "notesSegue",
            let dnc = segue.destination as? UINavigationController,
            let dvc = dnc.topViewController as? NotesViewController,
            let indexPath = foldersTableView.indexPathForSelectedRow
        {
            dvc.folderKey = (folders[indexPath.row].ref?.key)!
        }
    }
}


extension FoldersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return folders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Folder", for: indexPath)
        cell.textLabel?.text = folders[indexPath.row].title
        
        return cell
    }
}
