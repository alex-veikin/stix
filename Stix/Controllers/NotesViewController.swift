import UIKit
import Firebase

class NotesViewController: UIViewController {
    
    var user: User!
    var ref: DatabaseReference!
    var folderKey: String!
    var notes = [Note]()
    
    @IBOutlet weak var notesTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let currentUser = Auth.auth().currentUser else { return }
        self.user = User(user: currentUser)
        self.ref = Database.database().reference(withPath: "users/\(user!.uid)/folders/\(folderKey!)/notes")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.ref.observe(.value, with: { [weak self] snapshot in
            var notes = [Note]()
            for item in snapshot.children {
                let note = Note(snapshot: item as! DataSnapshot)
                notes.append(note as Note)
            }
            self?.notes = notes
            self?.notesTableView.reloadData()
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.ref.removeAllObservers()
    }

    @IBAction func backButtonPressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addPressed(_ sender: UIBarButtonItem) {
        let ac = UIAlertController(title: "Add note", message: "Add new note", preferredStyle: .alert)
        ac.addTextField(configurationHandler: { (field) in
            field.placeholder = "Title"
        })
        ac.addTextField(configurationHandler: { (field) in
            field.placeholder = "Text"
        })
        
        let saveButton = UIAlertAction(title: "Save", style: .default) { [weak self] _ in
            guard let titleField = ac.textFields?[0], !titleField.text!.isEmpty else { return }
            guard let textField = ac.textFields?[1], !textField.text!.isEmpty else { return }
            let note = Note(title: titleField.text!, text: textField.text!)
            let noteRef = self?.ref.childByAutoId()
            noteRef?.setValue(["title": note.title, "text": note.text])
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        ac.addAction(saveButton)
        ac.addAction(cancelButton)
        
        present(ac, animated: true, completion: nil)
    }
}

extension NotesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Note", for: indexPath)
        cell.textLabel?.text = notes[indexPath.row].title
        
        return cell
    }
}
