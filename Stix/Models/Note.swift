import Foundation
import Firebase

struct Note {
    let title: String
    let text: String
    let ref: DatabaseReference?
    
    init(title: String, text: String) {
        self.title = title
        self.text = text
        self.ref = nil
    }
    
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        self.title = snapshotValue["title"] as! String
        self.text = snapshotValue["text"] as! String
        self.ref = snapshot.ref
    }
}
