import Foundation
import Firebase

struct Folder {
    let title: String
    let ref: DatabaseReference?
    
    init(title: String) {
        self.title = title
        self.ref = nil
    }
    
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        self.title = snapshotValue["title"] as! String
        self.ref = snapshot.ref
    }
}
